use super::General;
use std::slice::Iter;

pub struct MatrixColumnIterator<'a, T> {
    iter: Iter<'a, T>,
}

impl<'a, T> MatrixColumnIterator<'a, T> {
    pub fn new(g: &'a General<T>, column: usize) -> MatrixColumnIterator<'a, T> {
        let rows = g.nrows();

        let first = column * rows;
        let iter = g.data[first..first + rows].iter();
        MatrixColumnIterator { iter }
    }
}

impl<'a, T> Iterator for MatrixColumnIterator<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}
