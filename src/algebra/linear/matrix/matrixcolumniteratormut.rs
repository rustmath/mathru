use super::General;
use std::slice::IterMut;

pub struct MatrixColumnIteratorMut<'a, T> {
    iter: IterMut<'a, T>,
}

impl<'a, T> MatrixColumnIteratorMut<'a, T> {
    pub fn new(g: &'a mut General<T>, column: usize) -> MatrixColumnIteratorMut<'a, T> {
        let rows = g.nrows();

        let first = column * rows;
        let iter = g.data[first..first + rows].iter_mut();
        MatrixColumnIteratorMut { iter }
    }
}

impl<'a, T> Iterator for MatrixColumnIteratorMut<'a, T> {
    type Item = &'a mut T;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}
