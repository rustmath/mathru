use super::General;
use std::iter::{Skip, StepBy, Take};
use std::slice::Iter;

pub struct MatrixRowIterator<'a, T> {
    iter: Take<StepBy<Skip<Iter<'a, T>>>>,
}

impl<'a, T> MatrixRowIterator<'a, T> {
    pub fn new(g: &'a General<T>, row: usize) -> MatrixRowIterator<'a, T> {
        let (m, n) = g.dim();
        let iter = g.data.iter().skip(row).step_by(n).take(m);
        return MatrixRowIterator { iter };
    }
}

impl<'a, T> Iterator for MatrixRowIterator<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}
