use criterion::Criterion;
use mathru::algebra::abstr::cast::FromPrimitive;
use mathru::algebra::linear::vector::Vector;

criterion_group!(
    bench_index,
    bench_vector_index,
    bench_vector_iter,
    bench_vec_index
);

const NUM: usize = 1000000;

fn bench_vector_index(bench: &mut Criterion) {
    let mut v = Vector::new_column(vec![1.0f64; NUM]);
    bench.bench_function("vector index", move |b| {
        b.iter(|| {
            for i in 0..NUM {
                v[i] = f64::from_u64(i as u64);
            }
        });
    });
}

fn bench_vector_iter(bench: &mut Criterion) {
    let mut v = Vector::new_column(vec![1.0f64; NUM]);
    bench.bench_function("vector iter", move |b| {
        b.iter(|| {
            v.iter_mut().enumerate().for_each(|(i, c)| {
                *c = f64::from_u64(i as u64);
            })
        });
    });
}

fn bench_vec_index(bench: &mut Criterion) {
    let mut v = vec![1.0f64; NUM];
    bench.bench_function("vec index", move |b| {
        b.iter(|| {
            for i in 0..NUM {
                v[i] = i as f64;
            }
        });
    });
}
