use criterion::Criterion;
use mathru::algebra::linear::matrix::General;

criterion_group!(
    bench_general_add_borrow_general,
    general_add_borrow_general,
    vec_add_own_vec,
    // faer_add
);

const DIM: usize = 10024;

fn general_add_borrow_general(bench: &mut Criterion) {
    bench.bench_function("general add borrow general", move |bh| {
        bh.iter(|| {
            let g1 = General::new(DIM, DIM, vec![1.0f64; DIM * DIM]);
            let g2 = g1.clone();
            let _ = &g1 + &g2;
        });
    });
}

fn vec_add_own_vec(bench: &mut Criterion) {
    bench.bench_function("vec add borrow vec", move |bh| {
        bh.iter(|| {
            let vec1: Vec<f64> = vec![3.0; DIM * DIM];
            let vec2 = vec1.clone();
            let _ = vec1
                .iter()
                .zip(&vec2)
                .map(|(a, b)| a + b)
                .collect::<Vec<f64>>();
        });
    });
}

// fn faer_add(bench: &mut Criterion) {
//     bench.bench_function("faer-core add borrow", move |bh| {
//         use faer_core::Mat;
//         bh.iter(|| {
//             let g1 = Mat::<f64>::from_fn(DIM, DIM, |_, _| 1.0f64);
//             let g2 = g1.clone();

//             let _ = &g1 + &g2;
//         });
//     });
// }
