use criterion::Criterion;
use mathru::algebra::linear::matrix::General;
use std::slice::Iter;

criterion_group!(
    matrix_column_iterator,
    matrix_column_iter_add,
    vec_iterator_add,
    matrix_column_iter_mul,
    vec_iterator_mul,
    matrix_column_iter_zip_mul,
    vec_iterator_zip_mul,
    matrix_column_iter_zip_mul_loop,
    vec_iterator_zip_mul_loop
);

const DIM: usize = 1024;

fn matrix_column_iter_add(bench: &mut Criterion) {
    bench.bench_function("matrix column iterator add", move |bh| {
        bh.iter(|| {
            let a: General<f64> = General::new(DIM, 1, vec![3.0; DIM]);
            let sum = a.column_iter(0).fold(0.0f64, |acc, c| acc + *c);
            assert_ne!(sum, 0.0)
        });
    });
}

fn vec_iterator_add(bench: &mut Criterion) {
    bench.bench_function("vec iterator add", move |bh| {
        bh.iter(|| {
            let a = vec![3.0; DIM];
            let sum = a.iter().fold(0.0f64, |acc, c| acc + *c);
            assert_ne!(sum, 0.0)
        });
    });
}

fn matrix_column_iter_mul(bench: &mut Criterion) {
    bench.bench_function("matrix column iterator mul", move |bh| {
        bh.iter(|| {
            let a: General<f64> = General::new(DIM, 1, vec![3.0; DIM]);
            let sum = a.column_iter(0).fold(0.0f64, |acc, c| acc + *c * *c);
            assert_ne!(sum, 0.0)
        });
    });
}

fn vec_iterator_mul(bench: &mut Criterion) {
    bench.bench_function("vec iterator mul", move |bh| {
        bh.iter(|| {
            let a = vec![3.0; DIM];
            let sum = a.iter().fold(0.0f64, |acc, c| acc + *c * *c);
            assert_ne!(sum, 0.0)
        });
    });
}

fn matrix_column_iter_zip_mul(bench: &mut Criterion) {
    bench.bench_function("matrix column iterator zip mul", move |bh| {
        let a: General<f64> = General::new(DIM, 1, vec![3.0; DIM]);
        bh.iter(|| {
            let sum = a
                .column_iter(0)
                .zip(a.column_iter(0))
                .fold(0.0f64, |acc, (c, d)| acc + *c * *d);
            assert_ne!(sum, 0.0)
        });
    });
}

fn vec_iterator_zip_mul(bench: &mut Criterion) {
    bench.bench_function("vec iterator zip mul", move |bh| {
        bh.iter(|| {
            let mut a = vec![3.0; DIM];
            let sum = a
                .iter()
                .zip(a.iter())
                .fold(0.0f64, |acc, (c, d)| acc + *c * *d);
            assert_ne!(sum, 0.0);
            a[1] = sum;
        });
    });
}

fn matrix_column_iter_zip_mul_loop(bench: &mut Criterion) {
    bench.bench_function("matrix column iterator zip mul loop", move |bh| {
        let mut a: General<f64> = General::new(DIM, DIM, vec![3.0; DIM * DIM]);
        bh.iter(|| {
            for i in 0..DIM {
                for j in 0..(i + 1) {
                    let sum = a
                        .column_iter(i)
                        .take(j)
                        .zip(a.column_iter(j).take(j))
                        .take(j)
                        .fold(0.0f64, |acc, (c, d)| acc + *c * *d);
                    if sum == 0.0 {
                        assert_eq!(sum, 0.0);
                    } else {
                        assert_ne!(sum, 0.0);
                    }
                    a[[i, j]] = sum;
                }
            }
        });
    });
}

fn vec_iterator_zip_mul_loop(bench: &mut Criterion) {
    pub trait ColummIter<T> {
        fn column_iter(&self, column: usize, rows: usize) -> MatrixColumnIterator<T>;
    }

    impl<T> ColummIter<T> for Vec<T> {
        fn column_iter(&self, column: usize, rows: usize) -> MatrixColumnIterator<T> {
            MatrixColumnIterator::new(self, column, rows)
        }
    }

    pub struct MatrixColumnIterator<'a, T> {
        iter: Iter<'a, T>,
    }

    impl<'a, T> MatrixColumnIterator<'a, T> {
        pub fn new(g: &'a Vec<T>, column: usize, rows: usize) -> MatrixColumnIterator<'a, T> {
            let first = column * rows;
            let iter = g[first..first + rows].iter();
            MatrixColumnIterator { iter }
        }
    }

    impl<'a, T> Iterator for MatrixColumnIterator<'a, T> {
        type Item = &'a T;

        fn next(&mut self) -> Option<Self::Item> {
            self.iter.next()
        }
    }

    bench.bench_function("vec iterator zip mul loop", move |bh| {
        let mut a = vec![3.0; DIM * DIM];
        bh.iter(|| {
            for i in 0..DIM {
                for j in 0..(i + 1) {
                    let sum = a
                        .column_iter(i, j)
                        .zip(a.column_iter(j, j))
                        .fold(0.0f64, |acc, (c, d)| acc + *c * *d);
                    a[j * DIM + i] = sum;
                }
            }
        });
    });
}
