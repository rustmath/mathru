use criterion::Criterion;
use mathru::algebra::linear::matrix::General;

criterion_group!(bench_general_mul, mul_matrix_own /*faer_mul*/,);

const DIM: usize = 100;

fn mul_matrix_own(bench: &mut Criterion) {
    bench.bench_function("mul matrix own", move |bh| {
        bh.iter(|| {
            let a: General<f64> = General::new(DIM, DIM, vec![3.0; DIM * DIM]);
            let b: General<f64> = General::new(DIM, DIM, vec![3.0; DIM * DIM]);
            let _ = a * b;
        });
    });
}

// fn faer_mul(bench: &mut Criterion) {
//     bench.bench_function("faer-core mul own ", move |bh| {
//         use faer_core::Mat;
//         bh.iter(|| {
//             let g1 = Mat::<f64>::from_fn(DIM, DIM, |_, _| 1.0f64);
//             let g2 = g1.clone();

//             let _ = g1 * g2;
//         });
//     });
// }
