use criterion::Criterion;
//use faer_core::Parallelism;
use mathru::algebra::linear::matrix::{CholeskyDecomposition, Diagonal, General};

criterion_group!(bench_cholesky, cholesky_dec /*faer_cholesky_dec*/,);

const DIM: usize = 1024;

fn cholesky_dec(bench: &mut Criterion) {
    let a: General<f64> = Diagonal::new(&[1.0; DIM]).into();

    bench.bench_function(
        format!("cholesky dec {}x{}", DIM, DIM).as_str(),
        move |bh| {
            bh.iter(|| {
                let _ = a.dec_cholesky();
            })
        },
    );
}

// fn faer_cholesky_dec(bench: &mut Criterion) {
//     use dyn_stack::{GlobalPodBuffer, PodStack, ReborrowMut};
//     use faer_core::Mat;
//     let mut a = Mat::<f64>::identity(DIM, DIM);

//     bench.bench_function(
//         format!("faer cholesky dec {}x{}", DIM, DIM).as_str(),
//         move |bh| {
//             let parallelism: Parallelism = Parallelism::None;
//             let mut mem = GlobalPodBuffer::new(
//                 faer_cholesky::llt::compute::cholesky_in_place_req::<f64>(
//                     DIM,
//                     parallelism,
//                     Default::default(),
//                 )
//                 .unwrap(),
//             );

//             bh.iter(|| {
//                 let mut stack = PodStack::new(&mut mem);
//                 faer_cholesky::llt::compute::cholesky_in_place(
//                     a.as_mut(),
//                     Default::default(),
//                     parallelism,
//                     stack.rb_mut(),
//                     Default::default(),
//                 )
//             })
//         },
//     );
// }
