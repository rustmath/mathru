use criterion::Criterion;
use mathru::algebra::linear::matrix::SubstituteBackward;
use mathru::algebra::linear::{
    matrix::{General, UpperTriangular},
    vector::Vector,
};
use rand::Rng;

criterion_group!(
    bench_substitute_backward,
    substitute_backward_1024_vector,
    substitute_backward_1024_matrix
);

fn substitute_backward_1024_vector(bench: &mut Criterion) {
    let n = 1024;
    let mut a: UpperTriangular<f64> = General::new_random(n, n).into();
    let mut rng = rand::thread_rng();
    for i in 0..n {
        a[[i, i]] = rng.gen_range(0.1..10.0)
    }

    bench.bench_function("substitute backwards 1024 vector", move |bh| {
        bh.iter(|| {
            let y: Vector<f64> = Vector::new_column(General::new_random(n, 1).convert_to_vec());
            let _ = a.substitute_backward(y);
        })
    });
}

fn substitute_backward_1024_matrix(bench: &mut Criterion) {
    let n = 1024;

    let mut a: UpperTriangular<f64> = UpperTriangular::new_random(n);
    let mut rng = rand::thread_rng();
    for i in 0..n {
        a[[i, i]] = rng.gen_range(0.1..10.0)
    }

    bench.bench_function("substitute backwards 1024 matrix", move |bh| {
        bh.iter(|| {
            let y: General<f64> = General::new_random(n, n);
            let _ = a.substitute_backward(y).unwrap();
        })
    });
}
