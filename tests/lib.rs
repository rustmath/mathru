#![allow(unstable_name_collisions)]

#[macro_use]
extern crate mathru;

mod algebra;
mod analysis;
mod elementary;
mod optimization;
mod special;
mod statistics;
